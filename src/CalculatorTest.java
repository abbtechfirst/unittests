import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testAddition() {
        assertEquals(5, calculator.add(2, 3));
        assertEquals(-1, calculator.add(2, -3));
        assertNotNull(calculator.add(1, 1));
    }

    @Test
    public void testSubtraction() {
        assertEquals(1, calculator.subtract(3, 2));
        assertEquals(5, calculator.subtract(2, -3));
        assertEquals(0, calculator.subtract(0, 0));
        assertNotNull(calculator.subtract(1, 1));
    }

    @Test
    public void testMultiplication() {
        assertEquals(6, calculator.multiply(2, 3));
        assertEquals(0, calculator.multiply(0, 5));
        assertEquals(6, calculator.multiply(-2, -3));
        assertNotNull(calculator.multiply(1, 1));
    }

    @Test
    public void testDivision() {
        assertEquals(2.0, calculator.divide(6, 3), 0.0001);
        assertEquals(-2.0, calculator.divide(6, -3), 0.0001);
        assertEquals(0.0, calculator.divide(0, 5), 0.0001);
        assertEquals(2.0, calculator.divide(-6, -3), 0.0001);
        assertThrows(ArithmeticException.class, () -> calculator.divide(1, 0));
    }

    @Test
    public void testAssertArrayEquals() {
        int[] expectedResults = {5, -1, 0};
        int[] actualResults = {
                calculator.add(2, 3),
                calculator.add(2, -3),
                calculator.add(0, 0),
        };
        assertArrayEquals(expectedResults, actualResults);
    }
    @Test
    public void testDivisionWithAssertArrayEquals() {
        double[] expectedResults = {2.0, -2.0, 0.0, 2.0};
        double[] actualResults = {
                calculator.divide(6, 3),
                calculator.divide(6, -3),
                calculator.divide(0, 5),
                calculator.divide(-6, -3)
        };
        assertArrayEquals(expectedResults, actualResults, 0.0001);
    }

    @Test
    public void testSameAndNotSame() {
        Calculator calc1 = new Calculator();
        Calculator calc2 = new Calculator();
        Calculator calc3 = calc1;

        assertSame(calc1, calc3);
        assertNotSame(calc1, calc2);
    }

    @Test
    public void testTrueFalse() {
        assertTrue(calculator.add(2, 3) == 5);
        assertFalse(calculator.add(2, 2) == 5);
    }

    @Test
    public void testNullNotNull() {
        assertNotNull(calculator);
        Calculator nullCalculator = null;
        assertNull(nullCalculator);
    }
}
